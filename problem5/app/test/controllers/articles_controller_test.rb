require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:articles)
  end
  def test_show
  	get :show, :id => articles(:article1)
    assert_not_nil assigns(:article)
    assert_response :success
  end
  def test_new
  	login_as('marko@gmail.com')
  	get :new
  	assert_not_nil assigns(:article)
  	assert_response :success
  end
  def test_create
  	login_as('marko@gmail.com')
  	 assert_difference('Article.count') do
      post :create, :article => {:title => 'new title', :descriptions => "new body", :user_id=>session[:user_id]}
      assert_not_nil assigns(:article)
            assert_equal assigns(:article).title, "new title"
            assert_equal assigns(:article).descriptions, "new body"
            assert_equal assigns(:article).valid?, true
    end
    assert_response :redirect
    assert_redirected_to article_path(assigns(:article))
        assert_equal flash[:notice], 'Article created'
  end
  def test_show
  	get :show, :id => Article.first.title
    assert_not_nil assigns(:article)
    assert_response :success
  end

  def test_show_with_no_or_undefined_title
    get :show, :id => Time.now.to_i
    assert_nil assigns(:article)
    assert_response :redirect
    assert_redirected_to articles_path
    assert_equal flash[:notice], 'Cannot find the post'
  end
  def test_show_user_edit_his_article
  	login_as('sultan@gmail.com')
    sultan=users(:sultan)
    get :edit, :id => sultan.articles.first
    assert_not_nil assigns(:article)
    assert_response :success
  end
  def test_show_user_edit_other_user_article
    login_as('sultan@gmail.com')
    sultan=users(:sultan)
    marko=users(:marko)
    get :edit, :id => marko.articles.first
    assert_not_nil assigns(:article)
    assert_response :redirect
    assert_redirected_to user_path(sultan)
    assert_equal flash[:notice], 'You have no access'
  end
  def test_user_update_his_article_invalid
    login_as('sultan@gmail.com')
    sultan=users(:sultan)
    put :update, :id => sultan.articles.first,
                 :article => {:title => 'updated title', :descriptions => nil}
    assert_not_nil assigns(:article)
    assert_template :edit
  end
  def test_user_update_other_user_article
    login_as('sultan@gmail.com')
    sultan=users(:sultan)
    marko=users(:marko)
    put :update, :id => marko.articles.first,
                 :article => {:title => 'updated title', :descriptions => "sultan articles"}
    assert_not_nil assigns(:article)
    assert_equal flash[:notice], 'You have no access'
    assert_response :redirect
    assert_redirected_to user_path(sultan)
    
  end
  def test_user_delete_his_article
    login_as('sultan@gmail.com')
    sultan=users(:sultan)
    assert_difference('Article.count', -1) do
      delete :destroy, :id => sultan.articles.first
      assert_not_nil assigns(:article)
    end
    assert_response :redirect
    assert_redirected_to user_path(sultan)
    assert_equal flash[:notice], 'Article deleted'
  end
  def test_user_delete_other_user_article
    login_as('sultan@gmail.com')
    sultan=users(:sultan)
    marko=users(:marko)
      delete :destroy, :id => marko.articles.first
      assert_not_nil assigns(:article)
    assert_response :redirect
    assert_redirected_to user_path(sultan)
    assert_equal flash[:notice], 'You have no access'
    
  end
end

