require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  def test_create
  	assert_difference('Comment.count') do
  		post :create, :format => 'js', :comment=>{:content=>'blabla'}
  		assert_not_nil assigns(:comment)
    	assert_equal assigns(:p).content, "blabla"
        assert_equal assigns(:p).valid?, true
  	end
  		assert_equal :notice => 'Comment was successfully created'
  end
end
