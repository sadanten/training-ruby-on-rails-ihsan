require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  def test_index
  	 get :index
    assert_response :success
    assert_not_nil assigns(:products)
  end
  def test_show_product
  	product=products(:one)
  	get :show, :id => product.name
    assert_not_nil assigns(:p)
    assert_response :success
  end
  def test_show_with_no_or_undefined_product
    get :show, :id => "unidentified product"
    assert_nil assigns(:p)
    assert_response :redirect
    assert_redirected_to products_path
    assert_equal flash[:notice], 'Cannot find the product'
  end
  def test_new
  	login_as('sultan@gmail.com')
  	get :new
  	assert_not_nil assigns(:p)
  	assert_response :success 
  end
  def test_create
  	login_as('sultan@gmail.com')
  	assert_difference('Product.count') do
    	post :create, :product => {:name => 'new product', :description => "new dsadasd", :user_id=>session[:user_id], :stock=>3, :price=>5000}
    	assert_not_nil assigns(:p)
    	assert_equal assigns(:p).name, "new product"
        assert_equal assigns(:p).description, "new dsadasd"
        assert_equal assigns(:p).valid?, true
    end
    assert_response :redirect
    assert_redirected_to product_path(assigns(:p))
        assert_equal flash[:notice], 'Product created'
  end

  def test_user_show_edit_his_product
  	login_as('sultan@gmail.com')
    sultan=users(:sultan)
    get :edit, :id => sultan.products.first
    assert_not_nil assigns(:p)
    assert_response :success
  end
  def test_user_show_edit_other_user_product
  	login_as('artyom@gmail.com')
    artyom=users(:artyom)
    marko=users(:marko)
    get :edit, :id => marko.products.first
    assert_not_nil assigns(:product)
    assert_response :redirect
    assert_redirected_to user_path(artyom)
    assert_equal flash[:notice], 'You have no access'
  end
  def test_user_update_his_product_invalid
  	login_as('sultan@gmail.com')
    sultan=users(:sultan)
    put :update, :id => sultan.products.first,
                 :product => {:name => 'products', :description => nil, stock: nil, price: nil}
    assert_not_nil assigns(:product)
    assert_template :edit
  end
  def test_user_update_other_user_product
  	login_as('sultan@gmail.com')
    sultan=users(:sultan)
    marko=users(:marko)
    put :update, :id => marko.products.first,
                 :product => {:name => 'products sultan', :description=> "sultan product", price: 200, stock: 2}
    assert_not_nil assigns(:product)
    assert_equal flash[:notice], 'You have no access'
    assert_response :redirect
    assert_redirected_to user_path(sultan)
  end
  def test_user_update_his_product_valid
  	login_as('sultan@gmail.com')
    sultan=users(:sultan)
    put :update, :id => sultan.products.first,
                 :product => {:name => 'products', :description => 'just', stock: 2, price: 200}
    assert_not_nil assigns(:product)
    sultan.products.first.reload
    assert_equal sultan.products.first.name, 'products'
    assert_equal sultan.products.first.description, 'just'
    assert_equal  sultan.products.first.stock.to_i, 2 
    assert_equal sultan.products.first.price.to_i, 200
    assert_response :redirect
    assert_redirected_to product_path(sultan.products.first)
    assert_equal flash[:notice], 'Product updated' 
  end
  def test_user_delete_his_product
  	login_as('sultan@gmail.com')
    sultan=users(:sultan)
    assert_difference('Product.count', -1) do
      delete :destroy, :id => sultan.products.first
      assert_not_nil assigns(:product)
    end
    assert_response :redirect
    assert_redirected_to user_path(sultan)
    assert_equal flash[:notice], 'Product deleted'
  end
  def test_user_delete_other_user_product
  	login_as('sultan@gmail.com')
    sultan=users(:sultan)
    marko=users(:marko)
      delete :destroy, :id => marko.products.first
      assert_not_nil assigns(:product)
    assert_response :redirect
    assert_redirected_to user_path(sultan)
    assert_equal flash[:notice], 'You have no access'
  end
end
