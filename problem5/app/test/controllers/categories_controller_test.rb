require 'test_helper'

class CategoriesControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  def test_index
  	get :index
    assert_response :success
    assert_not_nil assigns(:categories)
  end
  def test_show
  	category=categories(:category1)
  	get :show, :id => category.name
    assert_not_nil assigns(:c)
    assert_response :success
  end
  def show_with_unidentified_category
  	 get :show, :id => "unidentified category"
    assert_nil assigns(:c)
    assert_response :redirect
    assert_redirected_to categories_path
    assert_equal flash[:notice], 'Cannot find the category'
  end
end
