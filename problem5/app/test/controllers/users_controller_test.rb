require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  include FlexMock::TestCase
  
  def test_show
  	get :show, :id => users(:marko)
    assert_not_nil assigns(:user)
    assert_response :success
  end
  def test_show_undefined_user
  	get :show, :id => "anything can happen"
    assert_nil assigns(:user)
    assert_response :redirect
    assert_redirected_to articles_path
    assert_equal flash[:notice], 'Cannot find user'
  end
  def test_may_edit
  	login_as('sultan@gmail.com')
  	user=User.find(session[:user_id])
    get :edit, :id => user.username
    assert_not_nil assigns(:user)
    assert_response :success
  end
  def test_other_user_edit_other_user
  	login_as('sultan@gmail.com')
  	sultan=users(:sultan)
  	artyom=users(:artyom)
    get :edit, :id => artyom.username
    assert_not_nil assigns(:user)
    assert_response :redirect
    assert_redirected_to user_path(sultan.username)
    assert_equal flash[:notice], 'You have no access'
  end
  def test_user_may_update_valid
  	login_as('sultan@gmail.com')
  	sultan=users(:sultan)
  	put :update, :id => sultan.username, 
  	:user => {:first_name=>"alhari", last_name: "sucks", username:"sultan", email:"suck@gmail.com", password: "anything", country_id:1, address:"jakarta",age:24,date_of_birth:"12 January 1990"}
    assert_not_nil assigns(:user)
    sultan.reload
    assert_equal sultan.address, 'jakarta'
    assert_response :redirect
    assert_redirected_to user_path(assigns(:user))
    assert_equal flash[:notice], 'Successfully edited profile'
  end
  def test_user_may_update_not_valid
  	login_as('sultan@gmail.com')
  	sultan=users(:sultan)
  	put :update, :id => sultan.username, 
  	:user => {:first_name=>nil, last_name: nil, username:"sultan", email:nil, password: "anything", country_id:1, address:"jakarta",age:24,date_of_birth:"12 January 1990"}
  	assert_template :edit

  end
  def test_user_update_other_user
  	login_as('sultan@gmail.com')
  	sultan=users(:sultan)
  	artyom=users(:artyom)
  	put :update, id: artyom.username, :user => {:first_name=>"alhari", last_name: "sucks", username:"sultana", email:"suck@gmail.com", password: "anything", country_id:1, address:"jakarta",age:24,date_of_birth:"12 January 1990"}
  	assert_not_nil assigns(:user)
  	assert_response :redirect
  	assert_redirected_to user_path(sultan)
  	assert_equal flash[:notice], 'You have no access'
  end
  def test_user_reg_success
  	assert_difference('User.count') do
  		if !verify_recaptcha
  		post :create, :user=> {:first_name=>"alhari", last_name: "sucks", username:"asdf", email:"suck@gmail.com", password: "anything", country_id:1, address:"jakarta",age:24,date_of_birth:"12 January 1990"}
  		assert_not_nil assigns(:user)
  		assert_response :success
  		assert_redirected_to articles_path
  		assert_equal flash[:notice], 'You\'re signed up, please login first'
  		end
  	end
  end

end
