require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def test_create_without_title
  	user=User.find_by_username('marko')
  	assert_not_nil user
  	article=Article.create(:descriptions=>"lalalal batman", :user_id=>user)
  	assert !article.valid?
    assert !article.save
  end
  def test_create_without_desc
  	user=users(:marko)
  	assert_not_nil user
  	article=Article.create(:title=>"Batman keseleo", :user_id=>user)
  	assert !article.valid?
    assert !article.save
  end
  def test_relation_with_comment
  	user=users(:marko)
  	assert_not_nil user
  	article=Article.create(:title=>"Batman keseleo", :user_id=>user,:descriptions=>"lalalal batman")
    comment=Comment.create(:content=>"funny", :article_id=>article.id)
    assert_not_nil comment
    assert_not_nil article.comments
    assert_equal article.comments.empty?, false
    assert_equal article.comments[0].class, Comment
  end
  def test_relation_with_user
    a=articles(:article2)
    assert_not_nil a
    assert_not_nil a.user.username
    assert_equal a.user.class, User    
  end

  def test_scope_rating
  	a=Article.rate(4)
  	assert_not_nil a
  end

  def test_scope_rate_more_than
  	a=Article.rating_more_than(3)
  	assert_not_nil a
  end
  def test_scope_rate_less_than
  	a=Article.rating_less_than(5)
  	assert_not_nil a
  end
  def test_content_more_than_100_c
  	a=Article.content_more_than_100_c
  	assert_not_nil a  	
  end


end
