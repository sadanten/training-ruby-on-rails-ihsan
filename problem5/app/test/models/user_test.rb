require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  def test_save_without_first_name
  	user=User.new(:last_name=>"kovacevic", :email=>"kgmail.com", :username=>"kovacevic", :password=>"blanla",:date_of_birth=>"12 jan 1990", :country_id=>1,:age=>24,:admin=>false, :address=>"bandung")
  	assert !user.valid?
    assert !user.save
  end
  def test_save_without_last_name
  	user=User.new(:first_name=>"kovacevic", :email=>"kgmail.com", :username=>"kovacevic", :password=>"blanla",:date_of_birth=>"12 jan 1990", :country_id=>1,:age=>24,:admin=>false, :address=>"bandung")
  	assert !user.valid?
    assert !user.save
  end
  def test_save_without_email
  	user=User.new(:first_name=>"kovacevic", :last_name=>"bandai", :username=>"kovacevic", :password=>"blanla",:date_of_birth=>"12 jan 1990", :country_id=>1,:age=>24,:admin=>false, :address=>"bandung")
  	assert !user.valid?
    assert !user.save
  end
  def test_save_without_username
  	user=User.new(:last_name=>"kovacevic",:first_name=>"bandai", :email=>"kgmail.com", :password=>"blanla",:date_of_birth=>"12 jan 1990", :country_id=>1,:age=>24,:admin=>false, :address=>"bandung")
  	assert !user.valid?
    assert !user.save
  end
  def test_save_withour_password
  	user=User.new(:last_name=>"kovacevic", :email=>"kgmail.com", :username=>"kovacevic", :first_name=>"blanla",:date_of_birth=>"12 jan 1990", :country_id=>1,:age=>24,:admin=>false, :address=>"bandung")
  	assert !user.valid?
    assert !user.save
  end
  def test_save_without_date
  	user=User.new(:last_name=>"kovacevic", :email=>"kgmail.com", :username=>"kovacevic", :password=>"blanla",:first_name=>"bianca", :country_id=>1,:age=>24,:admin=>false, :address=>"bandung")
  	assert !user.valid?
    assert !user.save
  end
  def test_without_country
  	user=User.new(:last_name=>"kovacevic", :email=>"kgmail.com", :username=>"kovacevic", :password=>"blanla",:first_name=>"bianca", :date_of_birth=>"12 January 1990",:age=>24,:admin=>false, :address=>"bandung")
  	assert !user.valid?
    assert !user.save
  end
  def test_without_age
  	user=User.new(:last_name=>"kovacevic", :email=>"kgmail.com", :username=>"kovacevic", :password=>"blanla",:first_name=>"bianca", :country_id=>1,:date_of_birth=>"24 January 1990",:admin=>false, :address=>"bandung")
  	assert !user.valid?
    assert !user.save
  end
  def test_without_address
  	user=User.new(:last_name=>"kovacevic", :email=>"kgmail.com", :username=>"kovacevic", :password=>"blanla",:first_name=>"bianca", :country_id=>1,:age=>24,:admin=>false, :date_of_birth=>"12 January 1990")
  	assert !user.valid?
    assert !user.save
  end
  
  def test_find_idn
    country = Country.find_by_name("Indonesia")
    assert_not_nil country
    user = User.create(:last_name=>"gogo", :email=>"gogogmail.com", :username=>"gogo", :password=>"blanla",:first_name=>"gigi", :country_id=>country.id,:age=>24,:admin=>false, :date_of_birth=>"12 January 1990", :address=>"Bandung")
   	assert_not_nil user
    assert_not_nil User.idn
    assert_not_nil User.idn[0].country.name
  end
  def test_relation_with_articles
    user=User.find_by_username("marko")
    assert_not_nil user
    a = Article.create(:title => "new_title", :descriptions => "new content", :user_id=>user.id)
    assert_not_nil a
    assert_not_nil user.articles
    assert_equal user.articles.empty?, false
    assert_equal user.articles[0].class, Article
  end
  def test_show_full_address
    country = Country.find_by_name("Indonesia")
    user = User.create(:last_name=>"gogo", :email=>"gogogmail.com", :username=>"gogo", :password=>"blanla",:first_name=>"gigi", :country_id=>country.id,:age=>24,:admin=>false, :date_of_birth=>"12 January 1990", :address=>"Bandung")
    assert_not_nil user
    assert_not_nil user.show_full_address
  end

  def test_belongs_to_country
    a=users(:marko)
    assert_not_nil a
    assert_not_nil a.country
    assert_equal a.country.class, Country    
  end
  def test_has_many_products
    a=users(:marko)
    assert_not_nil a
    assert_not_nil a.products
    assert_equal a.products[0].class, Product
  end
  def test_show_full_address
    a=users(:marko).show_full_address
    assert_not_nil a
  end
  def test_is_admin
    a=users(:marko).is_admin?
    assert true
  end
  def test_age_old_gt_18
    a=User.age_old_gt_18
    assert_not_nil a
  end
  def test_encrypt_password
    user=User.new(:last_name=>"kovacevic", :email=>"kgmail.com", :username=>"kovacevic", :password=>"blanla",:first_name=>"bianca", :country_id=>1,:age=>24,:admin=>false, :address=>"bandung", date_of_birth: "12 January 1990")
    assert user.valid?
    assert user.save
    assert_not_nil user.password_salt
    assert_not_nil user.password_hash
    assert_equal user.password_hash, BCrypt::Engine.hash_secret(user.password,user.password_salt)
  end


end

