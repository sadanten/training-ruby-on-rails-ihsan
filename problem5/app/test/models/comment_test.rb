require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def test_insert_without_content
  	ar=articles(:article1)
  	a=Comment.create(:article_id=>ar.id)
  	assert !a.valid?
  	assert !a.save
  end

  def test_relation_with_article
  	c=comments(:comment1)
  	assert_not_nil c
  	assert_not_nil c.article
  	assert_equal  c.article.class, Article
  end
end
