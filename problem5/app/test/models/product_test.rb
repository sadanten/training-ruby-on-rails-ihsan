require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def test_insert_without_name
  	p=Product.new(:description=>"bla bla bla", price: 2, stock: 3)
  	assert !p.valid?
    assert !p.save 
  end
  def test_insert_without_price
  	p=Product.new(:description=>"bla bla bla", name: "suck product", stock: 3)
  	assert !p.valid?
    assert !p.save
  end
  def test_insert_without_stock
  	p=Product.new(:description=>"bla bla bla", name: "suck product", price: 3)
  	assert !p.valid?
    assert !p.save
  end
  def test_insert_without_desc
  	p=Product.new(:stock=>2, name: "suck product", price: 3)
  	assert !p.valid?
    assert !p.save
  end
  def test_scope_stock
  	c=20
  	p=Product.stock(c)
  	assert_not_nil p
  end
  def test_scope_more_than
  	c=10
  	p=Product.stock_more_than(c)
  	assert_not_nil p
  end
  def test_scope_less_than
  	c=30
  	p=Product.stock_less_than(c)
  	assert_not_nil p
  end
  def test_find_price_less_1000
  	p=Product.find_price_less_1000
  	assert_not_nil p
  end

end
