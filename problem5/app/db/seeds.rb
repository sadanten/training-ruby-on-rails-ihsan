# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create(first_name: "Salas", last_name: 'Marcelo',email: 'salas@gmail.com',username: 'salas', password: '827ccb0eea8a706c4c34a16891f84e7b', date_of_birth: '1 january 1990', age: 24, address:"Bandung")
User.create(first_name: "Batistuta", last_name: "Omar", email: "batigol@gmail.com", username: 'batigol', password: '827ccb0eea8a706c4c34a16891f84e7b', date_of_birth: '2 january 1990', age:24, address:'Bandung')
User.create(first_name: "Totti", last_name: "Fransesco", email: "totti@gmail.com", username: 'totti', password: '827ccb0eea8a706c4c34a16891f84e7b', date_of_birth: '3 january 1990', age:24, address:'Bandung')
User.create(first_name: "Maldini", last_name: "Christian", email: "maldini@gmail.com", username: 'maldini', password: '827ccb0eea8a706c4c34a16891f84e7b', date_of_birth: '5 january 1990', age:24, address:'Bandung')
User.create(first_name: "Montella", last_name: "Vicenzo", email: "montella@gmail.com", username: 'montella', password: '827ccb0eea8a706c4c34a16891f84e7b', date_of_birth: '4 january 1990', age:24, address:'Bandung')

1.upto(5) do |i|
Article.create(:title =>"Title #{i}",:body => "Content #{i}")
end

1.upto(5) do |i|
Comment.create(:content =>"comment #{i}")
end

Country.create(code: "IDN", name: "Indonesia")
Country.create(code: "UK", name: "United Kingdom")
Country.create(code: "AUS", name: "Australia")
Country.create(code: "US", name: "United States")
Country.create(code: "SING", name: "Singapore")


1.upto(5) do |i|
Product.create(name: "product #{i}", price: "#{i}", stock: "#{i}", description: "description #{i}", user_id:i)
end

1.upto(2) do |i|
Category.create(name: "Category #{i}", user_id:1)
end