class SomeChangeAgain < ActiveRecord::Migration
  def change
  	add_column :articles, :rating, :integer,  default: 0
  	
  	change_column :users, :address, :text
  	
  	rename_column :articles, :body, :descriptions
  end
end
