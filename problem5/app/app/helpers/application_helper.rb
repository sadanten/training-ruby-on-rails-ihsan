module ApplicationHelper

	def valid_error_display object
		if object.errors.any?
			@str=''
			object.errors.full_messages.each do |msg|
				@str +="<div class='row'> <div class='span12 alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button><p>#{msg}</p></div></div>"
			end
			return @str
		end
		
	end

	def show_current_user
		str = "" 
		  if current_user
		  	str="<li class='dropdown'><a data-toggle='dropdown' class='dropdown-toggle' href='#'>"
		  	str += " #{current_user.username}<strong class='caret'></strong>"
		  	str +="</a>"
		  	str += "<ul class='dropdown-menu'><li>"
		  	str += link_to("Profile", user_path(current_user.username))
		  	str +="</li>"
		  	str +="<li>"
		  	str +=link_to("Logout", log_out_path)
		  	str +="</li></ul></li>"
		  else 
		    str = "<li>" 
		    str += link_to("Signup", sign_up_path, "data-no-turbolink" => true)
		    str +="</li><li>" 
		    str += link_to "Login", log_in_path
		    str +="</li>"
		  end 
	end

	def message_notice_display
		str=""
		if flash[:notice]
			str += "<div class='row pull-right'><div class='span4 offset4 alert alert-block'><button type='button' class='close' data-dismiss='alert'>&times;</button>"
			str += "<p>#{flash[:notice]}</p></div></div>"
		end
	end
	def message_notice_header
		str=""
		if flash[:notice_header]
			str += "<div class='row'><div class='span12 alert alert-success'><button type='button' class='close' data-dismiss='alert'>&times;</button>"
			str += "<p>#{flash[:notice_header]}</p></div></div>"
		end
	end
	def message_notice_error_login
		str=""
		if flash[:error_login]
			str += "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button>"
			str += "<p>#{flash[:error_login]}</p></div>"
		end
	end
	def error_captcha
		str=""
		if flash[:errorcaptcha]
		str += "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button>"
		str += "<p>#{flash[:errorcaptcha]}</p></div>"
		end
	end
	def display_crud_for_user object,article
		str=""
		if current_user?(article.user)
		
			str +=link_to "Edit", edit_article_path(article)
			str +=link_to 'Delete', article_path(article), method: :delete, data: { confirm: 'Are you sure?' }
		end
	end

	def disp_crud_prod_user object,product
		str =""
		if current_user?(product.user)
			str +=link_to "Edit", edit_product_path(product)
			str +=link_to 'Delete', product_path(product), method: :delete, data: { confirm: 'Are you sure?' }
		end
	end
	def display_edit_profile object
		str=""
		if  current_user?(object)
		
			str +="<div class='span2'>"
			str +=link_to "Edit", edit_user_path(object), :class=>"btn btn-primary"
			str +="</div>"
		
		end
	end

	def display_menu
		str=""
		if current_user and current_user.admin == true 
			str +="<li>"
			str +=link_to "Articles", admin_articles_path
			str +="</li><li>"
			str +=link_to "Products", admin_products_path
			str +="</li><li>"
			str +=link_to "Categories", admin_categories_path
			str +="</li><li>"
			str +=link_to "Users", admin_users_path
			str +="</li><li>"
			str +=link_to "Countries", admin_countries_path
			str +="</li>"

		else
			str +="<li>"
			str +=link_to "Articles", articles_path
			str +="</li><li>"
			str +=link_to "Products", products_path
			str +="</li><li>"
			str +=link_to "Categories", categories_path
			str +="</li>"
		end
	end
	
end

  
 