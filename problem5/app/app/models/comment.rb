class Comment < ActiveRecord::Base
	validates :content, presence: {message: "please fill content field"}
	belongs_to :article
end
