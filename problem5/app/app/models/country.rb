class Country < ActiveRecord::Base
	validates :code, presence: {message: "please fill code field"}, inclusion: { in: %w(IDN USA CHN AUS HKG) }
	validates :name, presence: {message: "please fill name field"}

	has_many :users, inverse_of: :country
	has_many :user_indo, -> {where("country_id = 1")}, class_name: "User"
	
		def to_param
			name
		end
				
end
