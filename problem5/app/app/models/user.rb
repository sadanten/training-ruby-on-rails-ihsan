class User < ActiveRecord::Base
	attr_accessor :password
	
	before_save :encrypt_password

	validates :first_name, presence: {message: "Must fill this field"},  length:{maximum: 25, message: "Just 25 characters allowed"}
    validates :last_name, presence: {message: "please fill last name field"}, format:{ with: /\A[a-zA-Z]+\z/,
    message: "Only letters allowed" }, length:{maximum: 25, message: "Just 25 characters allowed"}
    validates :email, presence: {message: "please fill email field"}, uniqueness: {message: "Already taken"}
    validates :password, presence: {message: "please fill password field"}, confirmation: true
    validates :username, presence: {message: "please fill username field"}
    validates :date_of_birth, presence: {message: "please fill date of birth field"}
    validates :age, presence: {message: "please fill age field"}
    validates :address, presence: {message: "please fill address field"}
    validates :country, presence: {message: "please fill country field"}

	has_many :products
	has_many :articles
	belongs_to :country
	belongs_to :users_indonesia, -> { where("country_id = 1") }
	has_many :categories
	has_many :articles_title, :class_name => "Article", :foreign_key => "user_id" do
		def my_country
			self.where("articles.title like '%my country%'") 
			
		end
	end

	scope :idn, -> { joins(:country).where("countries.name like '%indonesia%'")}
	
	
	def show_full_address 
		a=self.address
		c=self.country.name
		"User has address #{a} #{c}"
	end
	def is_admin?
    	self.admin == true
  	end
	def self.age_old_gt_18
		where("age > 18")
	end

	def encrypt_password
		if password.present?
			self.password_salt = BCrypt::Engine.generate_salt
			self.password_hash = BCrypt::Engine.hash_secret(password,password_salt)
		end
	end
	def self.authenticate(email, password)
	  user = find_by_email(email)
	  if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
	    user
	  else
	    nil
	  end
	end
	def to_param
   		username
  	end

end
