class Category < ActiveRecord::Base
	validates :name, presence: {message: "please fill name field"}, uniqueness: true

	belongs_to :user
	has_many :categorizations
	has_many :products, :through => :categorizations
	has_many :products_shoes, -> { where("name like '%shoes%'")}, class_name: "Product"

	scope :books, lambda {where("name like '%book%'")}

	def to_param
   		name
  	end
end
