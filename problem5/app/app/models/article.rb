class Article < ActiveRecord::Base
	validates :title, presence: {message: "can't be blank", allow_blank: false, allow_nil: false}, uniqueness: true
	validates :descriptions, presence: {message: "can't be blank"}

	

	has_many :comments, dependent: :destroy
	belongs_to :user
	
	scope :rate, lambda {|r| where("rating = ?", r)}  
	scope :rating_more_than, lambda {|r| where("rating >= ?", r)}  
	scope :rating_less_than, lambda {|r| where("rating <= ?", r)}  

	def self.content_more_than_100_c
		where("length(descriptions) > 100")
	end
	def to_param
   		title
  	end

end
