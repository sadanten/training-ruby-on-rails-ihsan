class Product < ActiveRecord::Base
	validates :price, numericality: {message: "Just numeric allowed"}, presence: {message: "please fill price field"}
	validates :stock, presence: {message: "please fill stock field"}
	validates :description, presence: {message: "please fill description field"}
	validates :name, presence: {message: "please fill name field"}

	belongs_to :user
	has_many :categorizations
	has_many :categories, :through => :categorizations

	scope :stock, lambda {|stock| where("stock = ?", stock)}  
	scope :stock_more_than, lambda {|stock| where("stock > ?", stock)}  
	scope :stock_less_than, lambda {|stock| where("stock < ?", stock)} 
	scope :find_price_less_1000, where("price < 1000") 

	def to_param
   		name
  	end
end
