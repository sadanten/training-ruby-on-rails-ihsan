class CommentsController < ApplicationController
	skip_before_filter :verify_authenticity_token
	def create
		@article = Article.find_by_title(params[:article_id])
		@comment = Comment.new(comment_params)
		@comment.article = @article
		respond_to do |format|
			if @comment.save
				format.html{redirect_to(article_path(@article), :notice => 'Comment was successfully created')}
				format.js
			end
		end
	end

	private
	def comment_params
		params.require(:comment).permit(:content)
	end

end

