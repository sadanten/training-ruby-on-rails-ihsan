class ProductsController < ApplicationController
	before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]
	before_filter :find_product, :only => [:edit, :update, :show, :destroy]
	before_action :correct_product_owner,   :only => [:edit, :update, :destroy]
	def index
		@products=Product.all
	end

	def new
		@p=Product.new
	end

	def create
		@user = current_user
		@p = Product.new(p_params)
		@p.user=@user
		
		if @p.save 
			flash[:notice]="Product created"
			redirect_to @p
		else
			render 'new'
		end
	end

	def show
		@p=Product.find_by_name(params[:id])

	end

	def edit
		@p=Product.find_by_name(params[:id])
	end
	def update
		@p = Product.find_by_name(params[:id])
		if @p.update(p_params)
			flash[:notice]="Product updated"
			redirect_to @p
		else
			render 'edit'
		end
	end
	def destroy
		@product=Product.find_by_name(params[:id])
		@product.destroy
		flash[:notice]="Product deleted"
		redirect_to current_user
	end

	private
	def p_params
		params.require(:product).permit(:name, :description, :stock, :price, :category_ids => [])
	end
	def find_product
      @product = Product.find_by_name(params[:id])
      if @product.nil?
        flash[:notice] = "Cannot find the product"
        redirect_to products_url
      end
  	end
end
