class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user

  def require_login
    if current_user.nil?
      flash[:notice] = "You are not permitted, please login first"
      redirect_to log_in_path
    else
      return current_user
    end
  end

  def correct_user
      @user = User.find_by_username(params[:id])
      if !current_user?(@user)
        flash[:notice]="You have no access"
        redirect_to current_user
      else
        return current_user
      end 
    end

  def correct_owner
    @a = Article.find_by_title(params[:id]) or Product.find_by_name(params[:id])
    if !(current_user.id == @a.user_id)
      flash[:notice]="You have no access"
      redirect_to current_user
    else
      return current_user
    end
  end

    def correct_product_owner
    @a = Product.find_by_name(params[:id]) or Article.find_by_title(params[:id])
    if !(current_user.id == @a.user_id)
      flash[:notice]="You have no access"
      redirect_to current_user
    else
      return current_user
    end
  end

  private
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]  
  end
  def current_user? user
    user == current_user
  end



end
