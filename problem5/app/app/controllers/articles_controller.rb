class ArticlesController < ApplicationController
	before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]
	before_filter :find_article, :only => [:edit, :update, :show, :destroy]
	before_action :correct_owner,   :only => [:edit, :update, :destroy]
	def new
		@article=Article.new
	end
	def index
		@articles=Article.all.paginate(:page => params[:page], :per_page => 5)

	end

	def create
		@user = current_user
		@article=Article.new(article_params)
		@article.user=@user
		
		if @article.save 
			flash[:notice]="Article created"
			redirect_to @article
		else
			render 'new'
		end
	end
	def show
		@comment = @article.comments.build
	end
	def edit
		@article = Article.find_by_title(params[:id])
	end
	def update
		if @article.update(article_params)
			flash[:notice]="Article updated"
			redirect_to @article
		else
			render 'edit'
		end
	end
	def destroy
		@article.destroy
		flash[:notice]="Article deleted"
		redirect_to current_user
	end
	
	private 
	def article_params
		params.require(:article).permit(:title, :descriptions)
	end
	def find_article
      @article = Article.find_by_title(params[:id])
      if @article.nil?
        flash[:notice] = "Cannot find the post"
        redirect_to articles_url
      end
    end
	

	

end
