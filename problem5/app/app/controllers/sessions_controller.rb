class SessionsController < ApplicationController
  def new
  	render "login"
  end

  def create
  	u=User.authenticate(params[:email],params[:password])
  	if u
  		session[:user_id] = u.id
      session[:username] = u.username
      flash[:notice_header]="Welcome, #{u.first_name} #{u.last_name}"
  		if u.admin == true
        redirect_to  admin_articles_path
      else
        redirect_to root_url
      end 
  	else
  		flash[:error_login] = "Invalid email or password"
  		render "login"
  	end
  end

  def destroy
  	session[:user_id]=nil
  	flash[:notice] = "You have logged out"
  	redirect_to root_url
  end
end
