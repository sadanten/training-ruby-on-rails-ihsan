class CategoriesController < ApplicationController
	before_filter :find_category, :only => [:show]
	def index
		@categories=Category.all
	end

	def show
			
	end
	private
	def find_category
      @c = Category.find_by_name(params[:id])
      if @c.nil?
        flash[:notice] = "Cannot find the category"
        redirect_to categories_url
      end
  	end
end
