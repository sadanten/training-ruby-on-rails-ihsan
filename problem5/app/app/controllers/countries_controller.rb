class CountriesController < ApplicationController
 def new 
 	@a=Country.new
 end
 def index
 	@countries=Country.all
 end

 def create
 	@a = Country.new(c_params)	
	if @a.save 
		redirect_to @a
	else
		render 'new'
	end
 end

 def edit
 	@a=Country.find(params[:id])
 end

def update
	@a = Country.find(params[:id])
	if @a.update(c_params)
		redirect_to @a
	else
		render 'edit'
	end
end
def show
	@a=Country.find(params[:id])
end
def destroy
	@a=Country.find(params[:id])
	@a.destroy
	redirect_to countries_path
end

 private
 def c_params
 	params.require(:country).permit(:code, :name)
 end
 	
 

end
