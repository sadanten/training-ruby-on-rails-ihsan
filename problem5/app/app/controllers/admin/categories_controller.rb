class Admin::CategoriesController < Admin::ApplicationController
	before_filter :require_admin_login

	def index
		@categories=Category.all
	end
	def show
		@c=Category.find_by_name(params[:id])
		render 'categories/show'		
	end

	def new
		@c=Category.new
	end

	def create
		@user = User.find(session[:user_id])
		@c = Category.new(c_params)
		@c.user = @user
		if @c.save 
			flash[:notice]="New category successfully created"
			redirect_to admin_categories_path(@c)
		else
			render 'new'
		end
	end

	def edit
		@c=Category.find_by_name(params[:id])
	end
	def update
		@c = Category.find_by_name(params[:id])
		if @c.update(c_params)
			flash[:notice]="New category successfully updated"
			redirect_to admin_categories_path
		else
			render 'edit'
		end
	end

	def destroy
		@c = Category.find_by_name(params[:id])
		@c.destroy
		flash[:notice]="Category successfully deleted"
		redirect_to admin_categories_path
	end
	def c_params
		params.require(:category).permit(:name)
	end
end
