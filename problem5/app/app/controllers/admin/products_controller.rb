class Admin::ProductsController <  Admin::ApplicationController
	before_filter :require_admin_login

	def index
	 	@p=Product.all.paginate(:page => params[:page], :per_page => 5)
	end

	def show
		@p=Product.find_by_name(params[:id])
	end

	def new
	 	@p=Product.new
	end 

	def create
		@user = current_user
		@p = Product.new(p_params)
		@p.user=@user
		
		if @p.save 
			flash[:notice]="Product succesfully created"
			redirect_to admin_product_path(@p)
		else
			render 'new'
		end
	end

	def edit
		@p=Product.find_by_name(params[:id])
	end
	def update
		@p = Product.find_by_name(params[:id])
		if @p.update(p_params)
			flash[:notice]="Product succesfully updated"
			redirect_to @p
		else
			render 'edit'
		end
	end

	def destroy
		@product=Product.find_by_name(params[:id])
		@product.destroy
		redirect_to admin_products_path
	end

	def p_params
		params.require(:product).permit(:name, :description, :stock, :price, :category_ids => [])
	end
end
