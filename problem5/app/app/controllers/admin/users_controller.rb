class Admin::UsersController <  Admin::ApplicationController
	before_filter :require_admin_login

	def index
		@users=User.all
	end

	def show
		@a=User.find_by_username(params[:id])
		@ar=@a.articles
	end
	def edit
		@a=User.find_by_username(params[:id])
	end
	def update
		@a = User.find_by_username(params[:id])
		if @a.update(user_params)
			flash[:notice] = "Successfully edited profile"
			redirect_to admin_user_path(@a)
		else
			render 'edit'
		end
	end
	def destroy
		@a=User.find_by_username(params[:id])
		@a.destroy
		flash[:notice]="User successfully deleted"
		redirect_to admin_users_path
	end

end
