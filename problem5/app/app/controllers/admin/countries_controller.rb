class Admin::CountriesController < Admin::ApplicationController
	before_filter :require_admin_login

	def index
		@countries=Country.all
	end

	def show
		@a=Country.find_by_name(params[:id])
	end
	def new
		@a=Country.new
	end
	def create
		@a = Country.new(c_params)	
		if @a.save 
			flash[:notice]="Country successfully added"
			redirect_to admin_countries_path
		else
		render 'new'
		end
	end

	def edit
		@a=Country.find_by_name(params[:id])
	end
	def update
		@a = Country.find_by_name(params[:id])
		if @a.update(c_params)
			flash[:notice]="Country successfully edited"
			redirect_to admin_countries_path
		else
			render 'edit'
		end
	end
	def destroy
		@a=Country.find_by_name(params[:id])
		@a.destroy
		flash[:notice]="Country deleted"
		redirect_to admin_countries_path
	end
	private
	 def c_params
	 	params.require(:country).permit(:code, :name)
	 end
	 	
end
