class Admin::ArticlesController < Admin::ApplicationController
	before_filter :require_admin_login

	def index
		@articles=Article.select("articles.id, users.username,articles.title,articles.descriptions").joins(:user).paginate(:page => params[:page], :per_page => 5)
	end
	def new
		@article=Article.new
	end
	def create
		@user = User.find(session[:user_id])
		@article = Article.new(article_params)
		@article.user = @user
		
		if @article.save 
			redirect_to admin_article_path(@article)
		else
			render 'new'
		end
	end
	
	def edit
		@article = Article.find_by_title(params[:id])
	end
	def update
		@article = Article.find_by_title(params[:id])
		if @article.update(article_params)
			redirect_to admin_article_path(@article)
		else
			render 'edit'
		end
	end
	def destroy
		@article=Article.find_by_title(params[:id])
		if @article.destroy
			flash[:notice] = "Article successfully deleted"	
		else
			flash[:error] = "Delete failed"
		end
		redirect_to admin_articles_path
		
	end

	private 
	def article_params
		params.require(:article).permit(:title, :descriptions)
	end

end
