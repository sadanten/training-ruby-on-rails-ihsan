class UsersController < ApplicationController
	before_filter :require_login, :only => [:edit, :update]
	before_action :correct_user,   :only => [:edit, :update]
	
	def show
		@a=User.find_by_username(params[:id])
		c=@a.country.name
		@ar=@a.articles
	end

	def new
		@a=User.new
	end

	def create
		@a=User.new(user_params)
		if verify_recaptcha
		    if @a.save  
		    	UserMailer.registration_confirmation(@a).deliver
		      	flash[:notice] = "You're signed up, please login first"
		      redirect_to root_url  
		    else
		      render "new"  
		    end
		  	else
			    flash[:errorcaptcha] = "There was an error with the recaptcha code below.
			                     Please re-enter the code and click submit."
			    render "new"
		end
	end

	def edit

		@a=User.find_by_username(params[:id])
	end

	def update
		@a = User.find_by_username(params[:id])
		if @a.update(user_params)
			flash[:notice] = "Successfully edited profile"
			redirect_to @a
		else
			render 'edit'
		end
	end



	private 
	def user_params
		params.require(:user).permit(:first_name, :last_name, :email, :address, :username, :age, :date_of_birth, :country_id, :password, :admin)
	end
	


end
