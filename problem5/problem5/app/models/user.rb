class User < ActiveRecord::Base
	has_many :articles
	has_many :comments
	has_one :country
	has_many :products
	has_many :categories
end
