class Comment < ActiveRecord::Base
	belongs_to 	:user
	belongs_to 	:article,
				:class_name => "Article",
				:foreign_key => "id_article"
end
