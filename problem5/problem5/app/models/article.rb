class Article < ActiveRecord::Base
	belongs_to 	:user
	has_many 	:comments, dependent: :destroy, 
				:class_name => "Comment",
				:foreign_key => "id_article"

end
