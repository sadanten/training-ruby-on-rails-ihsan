class AddAssoc1 < ActiveRecord::Migration
  def change

  	create_table :categories_products, id:false do |t|
  	t.integer :category_id
  	t.integer :product_id
  	end

  	add_column :products, :id_user, :integer
  	add_column :articles, :id_user, :integer
  	add_column :comments, :id_user, :integer
  	add_column :comments, :id_article, :integer
  	add_column :users, :id_country, :integer
  	add_column :categories, :id_user, :integer

  end
end
