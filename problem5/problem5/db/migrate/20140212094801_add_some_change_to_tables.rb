class AddSomeChangeToTables < ActiveRecord::Migration
  def change

  	add_column :users, :date_of_birth, :string
  	add_column :users, :age, :integer
  	add_column :users, :address, :string
  	remove_column :users, :bio_profile
  	rename_column :users, :user_name, :username
  	rename_column :comments, :body, :content
  	change_column :countries, :code, :string
  	change_column :articles, :body, :text


  end
end
