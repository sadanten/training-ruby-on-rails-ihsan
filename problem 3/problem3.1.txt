2.1.0 :001 > def sorting(list)
2.1.0 :002?>   s=0
2.1.0 :003?>   loop do
2.1.0 :004 >       if list[s] == nil
2.1.0 :005?>       break
2.1.0 :006?>       else
2.1.0 :007 >         s += 1
2.1.0 :008?>       end
2.1.0 :009?>     end
2.1.0 :010?>   
2.1.0 :011 >     loop do
2.1.0 :012 >       swapped = false
2.1.0 :013?>     0.upto(s-2) do |i|
2.1.0 :014 >         if list[i] > list[i+1]
2.1.0 :015?>         list[i], list[i+1] = list[i+1], list[i]
2.1.0 :016?>         swapped = true
2.1.0 :017?>         end
2.1.0 :018?>       end
2.1.0 :019?>     break unless swapped
2.1.0 :020?>     end
2.1.0 :021?>   list
2.1.0 :022?>   end
 => :sorting 
2.1.0 :023 > a=[13,5,-2,1,4]
 => [13, 5, -2, 1, 4] 
2.1.0 :024 > r=sorting(a)
 => [-2, 1, 4, 5, 13] 
