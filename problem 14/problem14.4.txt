2.1.0 :047 > Product.create(name: "nananana", price: "dadada", stock: "dasda", description: "nananananaan")
   (0.2ms)  BEGIN
   (0.2ms)  ROLLBACK
 => #<Product id: nil, name: "nananana", price: "dadada", stock: "dasda", description: "nananananaan", created_at: nil, updated_at: nil, user_id: nil> 
2.1.0 :048 > Product.create(name: "nananana", price: 23, stock: "dasda", description: "nananananaan")
   (0.3ms)  BEGIN
  SQL (0.5ms)  INSERT INTO `products` (`created_at`, `description`, `name`, `price`, `stock`, `updated_at`) VALUES ('2014-02-14 17:21:52', 'nananananaan', 'nananana', 23, 'dasda', '2014-02-14 17:21:52')
   (50.6ms)  COMMIT
 => #<Product id: 6, name: "nananana", price: 23, stock: "dasda", description: "nananananaan", created_at: "2014-02-14 17:21:52", updated_at: "2014-02-14 17:21:52", user_id: nil> 
